
const conditionContainer = (props) => {
    return props.show ? props.children : "";
};

export default conditionContainer;
