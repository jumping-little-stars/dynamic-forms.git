// 密码校验
export const validatePass = (rule, value) => {
    if (value === "") {
        return Promise.resolve();
    } else {
        if (!/^(?:\d+|[a-zA-Z]+|[.!@#$%^&*]+){6,12}$/.test(value)) {
            return Promise.reject("请输入6-12位密码");
        } else {
            return Promise.resolve();
        }
    }
};
// 数字
export const validateNumber = (rule, value) => {
    if (value != "") {
        if (!/^[1-9]\d*$/.test(value)) {
            return Promise.reject("只能输入数字");
        } else {
            return Promise.resolve();
        }
    } else {
        return Promise.resolve();
    }
};
// 金额
export const validatePrice = (rule, value) => {
    if (value != "" && value != undefined && value != null) {
        let num = parseFloat(value);
        if (isNaN(num)) {
            return Promise.reject("请输入有效金额");
        } else {
            return Promise.resolve();
        }
    } else {
        return Promise.resolve();
    }
};
// 不作为
export const validateNone = () => {
    return Promise.resolve();
};
// 真实姓名
export const validateName = (rule, value) => {
    if (value === "" || !value) {
        return Promise.resolve();
    } else {
        if (!/^([\u4e00-\u9fa5]{1,20}|[a-zA-Z\.\s]{2,5})$/.test(value)) {
            return Promise.reject("请输入真实姓名");
        } else {
            return Promise.resolve();
        }
    }
};
// 身份证
export const validateIdCard = (rule, value) => {
    if (value != "" && value != undefined) {
        if (
            !/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(
                value
            )
        ) {
            return Promise.reject("请输入有效身份证");
        } else {
            return Promise.resolve();
        }
    } else {
        return Promise.resolve();
    }
};
//手机号
export const validatePhone = (rule, value) => {
    if (value != "" && value != undefined) {
        if (
            !/^1(3[0-9]|4[5,7]|5[0,1,2,3,5,6,7,8,9]|6[2,5,6,7]|7[0,1,7,8]|8[0-9]|9[1,8,9])\d{8}$/.test(
                value
            )
        ) {
            return Promise.reject("请输入有效手机号");
        } else {
            return Promise.resolve();
        }
    } else {
        return Promise.resolve();
    }
};
// 检测支付宝账号
export const validateZFB = (rule, value) => {
    if (
        /^([a-zA-Z\d])(\w|\-)+@[a-zA-Z\d]+\.[a-zA-Z]{2,4}$/.test(value) ||
        /^1[3456789]\d{9}$/.test(value)
    ) {
        return Promise.resolve();
    } else {
        return Promise.reject("请输入有效支付宝账号");
    }
};
// 邮箱
export const validateEmail = (rule, value) => {
    let email = /^([a-zA-Z\d])(\w|\-)+@[a-zA-Z\d]+\.[a-zA-Z]{2,4}$/.test(value);
    if (email) {
        return Promise.resolve();
    } else {
        return Promise.reject("请输入有效邮箱");
    }
};

export const OptionsValidate = [
    { label: "密码校验", value: "validatePass" },
    { label: "数字", value: "validateNumber" },
    { label: "金额", value: "validatePrice" },
    { label: "不限制", value: "validateNone" },
    { label: "真实姓名", value: "validateName" },
    { label: "身份证", value: "validateIdCard" },
    { label: "手机号", value: "validatePhone" },
    { label: "支付宝账号", value: "validateZFB" },
    { label: "邮箱", value: "validateEmail" },
];
