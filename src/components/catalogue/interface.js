

export const example = [
    {
        label: "单行文本", // 标签名称
        tips: "请输入", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "Input", // 控件类型
        value: "",
        allowClear: true, //是否允许清除-默认true
    },
    {
        label: "多行文本", // 标签名称
        tips: "请输入", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "InputTextera", // 控件类型
        value: "",
        allowClear: true, //是否允许清除-默认true
    },
    {
        label: "数字框", // 标签名称
        tips: "请输入", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "InputNumber", // 控件类型
        value: null,
    },
    {
        label: "下拉框", // 标签名称
        tips: "请输入", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "Select", // 控件类型
        options: [{ label: "选项1", value: 1 }], // 控件选项参数名称
        value: "",
    },
    {
        label: "单选", // 标签名称
        tips: "请输入", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "Radio", // 控件类型
        options: [{ label: "选项1", value: 1}], // 控件选项参数名称
        value: "",
    },
    {
        label: "复选框", // 标签名称
        tips: "请输入", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "Checkbox", // 控件类型
        options: [{ label: "选项1", value: 1 }], // 控件选项参数名称
        value: "",
    },
    {
        label: "开关", // 标签名称
        tips: "开启", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "Switch", // 控件类型
        value: false,
    },
    {
        label: "日期选择", // 标签名称
        tips: "请选择", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "DatePicker", // 控件类型
        value: "",
    },
    {
        label: "日期区间", // 标签名称
        tips: "请选择", // 提示语
        rule: "validateNone", // 需要限制规则名称
        type: "RangePicker", // 控件类型
        value: [],
    },
];
