import React, { useState } from "react";
import "./index.css";
import { example } from "./interface";

const Config = (props) => {
    const [list, setList] = useState(example);
    // fn 拖动开始
    const dragStartFn=(item)=> {
        // 自动识别配置必要参数
        let param = {
            ...item,
            required: false, //是否必填
        };
        props.setNewParams(param);
        props.setDragEnd(false);
    }
    // fn 拖动结束
    const dragEndFn=()=> {
        props.setDragEnd(true);
    }

    const renderList=()=> {
        return list.map((item, index) => {
            return (
                <div
                    onDragStart={()=>dragStartFn(item)}
                    className="catalogue-item"
                    draggable="true"
                    onDragEnd={dragEndFn}
                    key={index}
                >
                    {item.label}
                </div>
            );
        });
    }

    return (
        <div
            onDragEnter={(e) => {
                if (!props.valid) return;
                e.preventDefault();
                props.setValid(false);
            }}
            className="catalogue"
        >
            {renderList()}
        </div>
    );
};

export default Config;
