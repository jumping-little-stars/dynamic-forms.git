import React, { useEffect, useState } from 'react';
import Drag from "./Drag/index.jsx";

const Dynamicforms = (props) => {
  return (
    <div>
      <Drag
        getList={props.getlistChange}
        width={props?.width||1000}
        height={props?.height||800}
      />
    </div>
  );
};

export default Dynamicforms;
