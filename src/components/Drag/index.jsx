
import React, { useEffect, useState } from "react";
import "./index.css";
import UiForm from "../module/index.jsx";
import UiCatalogue from "../catalogue/index.jsx";
import UiConfig from "../config/index.jsx";
import { Button, Space, Form } from "antd";

const Drag = (props) => {
  const [form] = Form.useForm(null);
  const [list, setList] = useState([]); //列表
  const [newParams, setNewParams] = useState({}); //当前拖拽新增元素
  const [dragEnd, setDragEnd] = useState(true); //拖拽是否结束
  const [valid, setValid] = useState(false); //拖拽是否有效
  const [active, setActive] = useState(-1); //当前编辑控件下标

  const saveForm = () => {
    form.validateFields().then(res => {
      props.getlistChange(list)
    })
  }

  return (
    <div>
      <Space style={{
        position: 'absolute',
        left: '5px',
        top:'10px'
      }}>
        <Button type='primary' onClick={saveForm}>表单保存</Button>
        <Button onClick={() => setList([])}>表单重置</Button>
      </Space>
      <div className="drag"  style={{ width: props.width + 'px', height: props.height + 'px' }}>

        <UiCatalogue
          formList={list}
          setDragEnd={setDragEnd}
          setNewParams={setNewParams}
          setValid={setValid}
          valid={valid}
        />
        <UiForm
          setActive={setActive.bind(this)}
          setList={setList.bind(this)}
          formList={list}
          newParams={newParams}
          dragEnd={dragEnd}
          valid={valid}
          form={form}
          active={active}
          setValid={setValid}
        />
        <UiConfig
          setActive={setActive.bind(this)}
          active={active}
          formList={list}
          form={form}
          setList={setList.bind(this)}
        />
      </div>
    </div>
  );
};
export default Drag;
