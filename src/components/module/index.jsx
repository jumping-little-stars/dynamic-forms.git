import React, { useEffect, useState } from "react";
import moment from "moment";
import {
    Form,
    // Rate,
    Checkbox,
    Switch,
    Input,
    Select,
    Radio,
    InputNumber,
    DatePicker,
    // Slider,
} from "antd";
import * as validateParams from "../config/validate";
import "./index.css";

let draging = null;

const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;
const { RangePicker } = DatePicker;
let targetName = "drag-move"; //需要换位目标
let targetIndex = -1; //当前换位位置
let prevIndex = -1; //原坐标
const UiForm = (props) => {
    const {
        formList,
        newParams,
        setList,
        dragEnd,
        valid,
        setValid,
        setActive,
        active,
    } = props;

    useEffect(()=>{
        renderForm(props.formList)
    },[props.formList])

    // render 表单
    function renderForm(arr) {
        return arr.map((item, index) => {
            if (!item || !item.label) return;
            return (
                <div
                    key={item.key}
                    onClick={setActive.bind(this, index)}
                    onDragStart={dragStartFn.bind(this, index)}
                    onDragOver={dragOverFn.bind(this, index)}
                    onDragEnd={dragEndFn.bind(this)}
                    draggable
                    className={`drag-move drag-item drag-item-${active == index ? "active" : ""
                        }`}
                >
                    <Form.Item
                        label={item.label}
                        name={index}
                        hasFeedback
                        rules={[
                            {
                                required: item.required,
                                message: item.tips,
                            },
                            {
                                validator: validateParams[item.rule],
                            },
                        ]}
                    >
                        {renderSwatch(item, index)}
                    </Form.Item>
                </div>
            );
        });
    }
    // render 表单类型判定
    function renderSwatch(item, index) {
        switch (item.type) {
            case "Input":
                return (
                    <Input
                        allowClear={
                            item.allowClear === undefined
                                ? true
                                : item.allowClear
                        }
                        onChange={checkNoSpace.bind(this, index)}
                        placeholder={item.tips}
                        key={item.key}
                        value={item.value}
                  />
                );
            case 'InputTextera':
                return (
                    <Input.TextArea
                        allowClear={
                            item.allowClear === undefined
                                ? true
                                : item.allowClear
                        }
                        onChange={checkNoSpace.bind(this, index)}
                        placeholder={item.tips}
                        value={item.value}
                        rows={5}
                    />
                );
            case "InputNumber":
                return (
                    <InputNumber
                        min={item.min}
                        onChange={checkNoSpace.bind(this, index)}
                        placeholder={item.tips}
                        key={item.key}
                        style={{width:'50%'}}
                        value={item.value}
                    />
                );
            case "Select":
                return (
                    <Select
                        showSearch
                        mode={item.mode}
                        onChange={checkNoSpace.bind(this, index)}
                        placeholder={item.tips}
                        allowClear={
                            item.allowClear === undefined
                                ? true
                                : item.allowClear
                        }
                        value={item.value}
                        key={item.key}
                        filterOption={(input, option) =>
                            (option?.children).includes(
                                input
                            )
                        }
                    >
                        {item.options.map((its) => {
                            return (
                                <Option key={its.value} value={its.value}>
                                    {" "}
                                    {its.label}{" "}
                                </Option>
                            );
                        })}{" "}
                    </Select>
                );
            case "Radio":
                return (
                    <Radio.Group
                        onChange={checkNoSpace.bind(this, index)}
                        key={item.key}
                        value={item.value}
                    >
                        {item.options.map((its) => {
                            return (
                                <Radio key={its.value} value={its.value}>
                                    {" "}
                                    {its.label}{" "}
                                </Radio>
                            );
                        })}
                    </Radio.Group>
                );
            case "Checkbox":
                return (
                    <CheckboxGroup
                        onChange={checkNoSpace.bind(this, index)}
                        options={item.options}
                        value={item.value}
                    />
                );
            // case "Rate":
            //     return (
            //         <Rate
            //             allowClear={
            //                 item.allowClear === undefined
            //                     ? true
            //                     : item.allowClear
            //             }
            //             onChange={checkNoSpace.bind(this, index)}
            //             key={item.key}
            //             allowHalf
            //             value={item.value}
            //         />
            //     );
            case "Switch":
                return (
                    <Switch
                        onChange={checkNoSpace.bind(this, index)}
                        checked={item.value}
                        key={item.key}
                    />
                );
            // case "Slider":
            //     return (
            //         <Slider
            //             onChange={checkNoSpace.bind(this, index)}
            //             value={item.value}
            //             disabled={item.disabled}
            //         />
            //     );
            case "DatePicker":
                return (
                    <DatePicker
                        onChange={checkSetTime.bind(this, index)}
                        placeholder={item.tips}
                        key={item.key}
                        value={item.value}
                    />
                );
            case "RangePicker":
                return (
                    <RangePicker
                        format={item.format ? item.format : "YY-MM-DD HH:mm:ss"}
                        showTime
                        allowClear={
                            item.allowClear === undefined
                                ? true
                                : item.allowClear
                        }
                        value={item.value}
                        showNow={false}
                        key={item.key}
                        placeholder={item.tips}
                        onChange={checkSetArrTime.bind(this, index)}
                        onOk={checkSetArrTime.bind(this, index)}
                    />
                );
            default:
                return "";
        }
    }
    // fn 拖动开始
    function dragStartFn(index, e) {
        e.dataTransfer.setData("te", e.target.innerText); //不能使用text，firefox会打开新tab
        draging = e.target;
        prevIndex = index;
    }
    // fn 拖动中
    function dragOverFn(index, e) {
        e.preventDefault();
        let target = getParentNode(e.target);
        if (!target || target.className.includes(targetName)) return;
        if (target !== draging) {
            targetIndex = index;
        }
    }
    // fn 拖动结束
    function dragEndFn() {
        if (prevIndex === -1 || targetIndex === -1) return;
        let newList = [...formList];
        let prev = newList[prevIndex];
        let target = newList[targetIndex];
        newList.splice(prevIndex, 1, target);
        newList.splice(targetIndex, 1, prev);
        setList(newList);
    }

      // 数组转对象
      function arrToObj1(arr,idx){
        return arr.reduce((obj,item,index) => {
            if(index==idx){
                obj[index]=''
            }else{
                obj[index] = item
            }
             return obj
         },{})
     }
    // fn 新生成元素
    function dargAddFn() {
        let newForm=Object.values(props.form.getFieldValue())
        let setform=arrToObj1(newForm,formList.length)
        props.form.setFieldsValue({
            ...setform
          });
        // 直接新增
        if (!dragEnd || !newParams.label) return;
            var code = '' + (parseInt(Math.random()*1000000)+1000000);
            code = code.substring(1, 7);             
            newParams.key=newParams.type+code
            setList([...formList, newParams]);
    }
    // 阶梯查询父级元素
    function getParentNode(el) {
        try {
            if (el.className === "") return getParentNode(el.parentNode);
            if (el.className.includes(targetName)) return el;
            let parentName = el.parentNode.className;
            if (parentName.includes(targetName))
                return getParentNode(el.parentNode);
            if (!parentName.includes(targetName)) return el.parentNode;
        } catch (error) {
            console.log(error);
            console.log(el.className);
            return undefined;
        }
    }
    // 时间选择传值
    function checkSetTime(index, val) {
        let newList = [...formList];
        newList[index].value = moment(val._d).format("YYYY-MM-DD");
        setList(newList);
    }
    // 时间区间选择传值
    function checkSetArrTime(index, val) {
        let param;
        if (val === null) param = val;
        else if (val[1] != null) param = moment(val[1]._d).format("YYYY-MM-DD");
        else param = moment(val[0]._d).format("YYYY-MM-DD");

        let newList = [...formList];
        newList[index].value = param;
        setList(newList);
    }
    // 表单传值
    function checkNoSpace(index, e) {
        let param = e && e.target ? e.target.value : e;
        let newList = [...formList];
        newList[index].value = param;
        setList(newList);
    }

    useEffect(() => {
        if (valid) dargAddFn();
        if (dragEnd) {
            draging = null;
            targetIndex = -1;
        }
    }, [dragEnd]);

    return (
        <div
            onDragEnter={(e) => {
                if (valid) return;
                e.preventDefault();
                setValid(true);
            }}
            id="uiform"
            className="uiform"
        >
            <Form form={props.form}>{renderForm(formList)}</Form>
        </div>
    );
};
export default UiForm;
