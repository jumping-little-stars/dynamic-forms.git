'use strict';

var React = require('react');
var moment = require('moment');
var antd = require('antd');
var icons = require('@ant-design/icons');

function _iterableToArrayLimit(arr, i) {
  var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"];
  if (null != _i) {
    var _s,
      _e,
      _x,
      _r,
      _arr = [],
      _n = !0,
      _d = !1;
    try {
      if (_x = (_i = _i.call(arr)).next, 0 === i) {
        if (Object(_i) !== _i) return;
        _n = !1;
      } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0);
    } catch (err) {
      _d = !0, _e = err;
    } finally {
      try {
        if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return;
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
}
function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    enumerableOnly && (symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    })), keys.push.apply(keys, symbols);
  }
  return keys;
}
function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = null != arguments[i] ? arguments[i] : {};
    i % 2 ? ownKeys(Object(source), !0).forEach(function (key) {
      _defineProperty(target, key, source[key]);
    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) {
      Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
    });
  }
  return target;
}
function _defineProperty(obj, key, value) {
  key = _toPropertyKey(key);
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}
function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}
function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}
function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}
function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}
function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _toPrimitive(input, hint) {
  if (typeof input !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (typeof res !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}
function _toPropertyKey(arg) {
  var key = _toPrimitive(arg, "string");
  return typeof key === "symbol" ? key : String(key);
}

function styleInject(css, ref) {
  if (ref === void 0) ref = {};
  var insertAt = ref.insertAt;
  if (!css || typeof document === 'undefined') {
    return;
  }
  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';
  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }
  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css_248z$3 = ".drag {\r\n    padding: 5px;\r\n    display: flex;\r\n    margin-top: 50px;\r\n}\r\n.drag   .ant-col {\r\n    background: none;\r\n    width: 85px;\r\n}\r\n.drag  .uiform {\r\n    border: 1px solid #f1e8e8;\r\n        min-height: 50px;\r\n        width: 52%;\r\n        padding: 10px 50px;\r\n}\r\n";
styleInject(css_248z$3);

// 密码校验
var validatePass = function validatePass(rule, value) {
  if (value === "") {
    return Promise.resolve();
  } else {
    if (!/^(?:\d+|[a-zA-Z]+|[.!@#$%^&*]+){6,12}$/.test(value)) {
      return Promise.reject("请输入6-12位密码");
    } else {
      return Promise.resolve();
    }
  }
};
// 数字
var validateNumber = function validateNumber(rule, value) {
  if (value != "") {
    if (!/^[1-9]\d*$/.test(value)) {
      return Promise.reject("只能输入数字");
    } else {
      return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
};
// 金额
var validatePrice = function validatePrice(rule, value) {
  if (value != "" && value != undefined && value != null) {
    var num = parseFloat(value);
    if (isNaN(num)) {
      return Promise.reject("请输入有效金额");
    } else {
      return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
};
// 不作为
var validateNone = function validateNone() {
  return Promise.resolve();
};
// 真实姓名
var validateName = function validateName(rule, value) {
  if (value === "" || !value) {
    return Promise.resolve();
  } else {
    if (!/^([\u4e00-\u9fa5]{1,20}|[a-zA-Z\.\s]{2,5})$/.test(value)) {
      return Promise.reject("请输入真实姓名");
    } else {
      return Promise.resolve();
    }
  }
};
// 身份证
var validateIdCard = function validateIdCard(rule, value) {
  if (value != "" && value != undefined) {
    if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(value)) {
      return Promise.reject("请输入有效身份证");
    } else {
      return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
};
//手机号
var validatePhone = function validatePhone(rule, value) {
  if (value != "" && value != undefined) {
    if (!/^1(3[0-9]|4[5,7]|5[0,1,2,3,5,6,7,8,9]|6[2,5,6,7]|7[0,1,7,8]|8[0-9]|9[1,8,9])\d{8}$/.test(value)) {
      return Promise.reject("请输入有效手机号");
    } else {
      return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
};
// 检测支付宝账号
var validateZFB = function validateZFB(rule, value) {
  if (/^([a-zA-Z\d])(\w|\-)+@[a-zA-Z\d]+\.[a-zA-Z]{2,4}$/.test(value) || /^1[3456789]\d{9}$/.test(value)) {
    return Promise.resolve();
  } else {
    return Promise.reject("请输入有效支付宝账号");
  }
};
// 邮箱
var validateEmail = function validateEmail(rule, value) {
  var email = /^([a-zA-Z\d])(\w|\-)+@[a-zA-Z\d]+\.[a-zA-Z]{2,4}$/.test(value);
  if (email) {
    return Promise.resolve();
  } else {
    return Promise.reject("请输入有效邮箱");
  }
};
var OptionsValidate = [{
  label: "密码校验",
  value: "validatePass"
}, {
  label: "数字",
  value: "validateNumber"
}, {
  label: "金额",
  value: "validatePrice"
}, {
  label: "不限制",
  value: "validateNone"
}, {
  label: "真实姓名",
  value: "validateName"
}, {
  label: "身份证",
  value: "validateIdCard"
}, {
  label: "手机号",
  value: "validatePhone"
}, {
  label: "支付宝账号",
  value: "validateZFB"
}, {
  label: "邮箱",
  value: "validateEmail"
}];

var validateParams = /*#__PURE__*/Object.freeze({
  __proto__: null,
  OptionsValidate: OptionsValidate,
  validateEmail: validateEmail,
  validateIdCard: validateIdCard,
  validateName: validateName,
  validateNone: validateNone,
  validateNumber: validateNumber,
  validatePass: validatePass,
  validatePhone: validatePhone,
  validatePrice: validatePrice,
  validateZFB: validateZFB
});

var css_248z$2 = "\r\n.drag-item {\r\n    transition: all 0.3s;\r\n    margin: 5px 0;\r\n    cursor: move;\r\n\r\n}\r\n.uiform{\r\n    overflow: auto;\r\n}\r\n\r\n.uiform .ant-form-item-control-input-content,.uiform .ant-select-selection-placeholder{\r\n    text-align: left;\r\n}\r\n.uiform .ant-form-item .ant-form-item-explain-error{\r\n    text-align: left;\r\n}\r\n\r\n.uiform  .ant-input-textarea-affix-wrapper .ant-input-clear-icon{\r\n            position: absolute;\r\n            right: 30px !important;\r\n  }\r\n  .uiform  .ant-input-textarea-suffix .ant-form-item-feedback-icon{\r\n        \r\n            position: absolute;\r\n            top: 6px;\r\n            right: 0px;\r\n            font-size: 14px;\r\n        }\r\n\r\n\r\n/*定义滚动条高宽及背景 高宽分别对应横竖滚动条的尺寸*/\r\n::-webkit-scrollbar {\r\n    width: 8px;\r\n    /*滚动条宽度*/\r\n    height: 8px;\r\n    /*滚动条高度*/\r\n    background-color: white;\r\n  }\r\n  \r\n  /*定义滑块 内阴影+圆角*/\r\n  ::-webkit-scrollbar-thumb {\r\n    box-shadow: inset 0 0 0px white;\r\n    -webkit-box-shadow: inset 0 0 0px white;\r\n    background-color: rgb(193, 193, 193);\r\n    /*滚动条的背景颜色*/\r\n    border-radius: 20px;\r\n  }";
styleInject(css_248z$2);

var draging$1 = null;
var Option$1 = antd.Select.Option;
var CheckboxGroup = antd.Checkbox.Group;
var RangePicker = antd.DatePicker.RangePicker;
var targetName$1 = "drag-move"; //需要换位目标
var targetIndex$1 = -1; //当前换位位置
var prevIndex$1 = -1; //原坐标
var UiForm = function UiForm(props) {
  var formList = props.formList,
    newParams = props.newParams,
    setList = props.setList,
    dragEnd = props.dragEnd,
    valid = props.valid,
    setValid = props.setValid,
    setActive = props.setActive,
    active = props.active;
  React.useEffect(function () {
    renderForm(props.formList);
  }, [props.formList]);

  // render 表单
  function renderForm(arr) {
    var _this = this;
    return arr.map(function (item, index) {
      if (!item || !item.label) return;
      return /*#__PURE__*/React.createElement("div", {
        key: item.key,
        onClick: setActive.bind(_this, index),
        onDragStart: dragStartFn.bind(_this, index),
        onDragOver: dragOverFn.bind(_this, index),
        onDragEnd: dragEndFn.bind(_this),
        draggable: true,
        className: "drag-move drag-item drag-item-".concat(active == index ? "active" : "")
      }, /*#__PURE__*/React.createElement(antd.Form.Item, {
        label: item.label,
        name: index,
        hasFeedback: true,
        rules: [{
          required: item.required,
          message: item.tips
        }, {
          validator: validateParams[item.rule]
        }]
      }, renderSwatch(item, index)));
    });
  }
  // render 表单类型判定
  function renderSwatch(item, index) {
    switch (item.type) {
      case "Input":
        return /*#__PURE__*/React.createElement(antd.Input, {
          allowClear: item.allowClear === undefined ? true : item.allowClear,
          onChange: checkNoSpace.bind(this, index),
          placeholder: item.tips,
          key: item.key,
          value: item.value
        });
      case 'InputTextera':
        return /*#__PURE__*/React.createElement(antd.Input.TextArea, {
          allowClear: item.allowClear === undefined ? true : item.allowClear,
          onChange: checkNoSpace.bind(this, index),
          placeholder: item.tips,
          value: item.value,
          rows: 5
        });
      case "InputNumber":
        return /*#__PURE__*/React.createElement(antd.InputNumber, {
          min: item.min,
          onChange: checkNoSpace.bind(this, index),
          placeholder: item.tips,
          key: item.key,
          style: {
            width: '50%'
          },
          value: item.value
        });
      case "Select":
        return /*#__PURE__*/React.createElement(antd.Select, {
          showSearch: true,
          mode: item.mode,
          onChange: checkNoSpace.bind(this, index),
          placeholder: item.tips,
          allowClear: item.allowClear === undefined ? true : item.allowClear,
          value: item.value,
          key: item.key,
          filterOption: function filterOption(input, option) {
            return (option === null || option === void 0 ? void 0 : option.children).includes(input);
          }
        }, item.options.map(function (its) {
          return /*#__PURE__*/React.createElement(Option$1, {
            key: its.value,
            value: its.value
          }, " ", its.label, " ");
        }), " ");
      case "Radio":
        return /*#__PURE__*/React.createElement(antd.Radio.Group, {
          onChange: checkNoSpace.bind(this, index),
          key: item.key,
          value: item.value
        }, item.options.map(function (its) {
          return /*#__PURE__*/React.createElement(antd.Radio, {
            key: its.value,
            value: its.value
          }, " ", its.label, " ");
        }));
      case "Checkbox":
        return /*#__PURE__*/React.createElement(CheckboxGroup, {
          onChange: checkNoSpace.bind(this, index),
          options: item.options,
          value: item.value
        });
      // case "Rate":
      //     return (
      //         <Rate
      //             allowClear={
      //                 item.allowClear === undefined
      //                     ? true
      //                     : item.allowClear
      //             }
      //             onChange={checkNoSpace.bind(this, index)}
      //             key={item.key}
      //             allowHalf
      //             value={item.value}
      //         />
      //     );
      case "Switch":
        return /*#__PURE__*/React.createElement(antd.Switch, {
          onChange: checkNoSpace.bind(this, index),
          checked: item.value,
          key: item.key
        });
      // case "Slider":
      //     return (
      //         <Slider
      //             onChange={checkNoSpace.bind(this, index)}
      //             value={item.value}
      //             disabled={item.disabled}
      //         />
      //     );
      case "DatePicker":
        return /*#__PURE__*/React.createElement(antd.DatePicker, {
          onChange: checkSetTime.bind(this, index),
          placeholder: item.tips,
          key: item.key,
          value: item.value
        });
      case "RangePicker":
        return /*#__PURE__*/React.createElement(RangePicker, {
          format: item.format ? item.format : "YY-MM-DD HH:mm:ss",
          showTime: true,
          allowClear: item.allowClear === undefined ? true : item.allowClear,
          value: item.value,
          showNow: false,
          key: item.key,
          placeholder: item.tips,
          onChange: checkSetArrTime.bind(this, index),
          onOk: checkSetArrTime.bind(this, index)
        });
      default:
        return "";
    }
  }
  // fn 拖动开始
  function dragStartFn(index, e) {
    e.dataTransfer.setData("te", e.target.innerText); //不能使用text，firefox会打开新tab
    draging$1 = e.target;
    prevIndex$1 = index;
  }
  // fn 拖动中
  function dragOverFn(index, e) {
    e.preventDefault();
    var target = getParentNode(e.target);
    if (!target || target.className.includes(targetName$1)) return;
    if (target !== draging$1) {
      targetIndex$1 = index;
    }
  }
  // fn 拖动结束
  function dragEndFn() {
    if (prevIndex$1 === -1 || targetIndex$1 === -1) return;
    var newList = _toConsumableArray(formList);
    var prev = newList[prevIndex$1];
    var target = newList[targetIndex$1];
    newList.splice(prevIndex$1, 1, target);
    newList.splice(targetIndex$1, 1, prev);
    setList(newList);
  }

  // 数组转对象
  function arrToObj1(arr, idx) {
    return arr.reduce(function (obj, item, index) {
      if (index == idx) {
        obj[index] = '';
      } else {
        obj[index] = item;
      }
      return obj;
    }, {});
  }
  // fn 新生成元素
  function dargAddFn() {
    var newForm = Object.values(props.form.getFieldValue());
    var setform = arrToObj1(newForm, formList.length);
    props.form.setFieldsValue(_objectSpread2({}, setform));
    // 直接新增
    if (!dragEnd || !newParams.label) return;
    var code = '' + (parseInt(Math.random() * 1000000) + 1000000);
    code = code.substring(1, 7);
    newParams.key = newParams.type + code;
    setList([].concat(_toConsumableArray(formList), [newParams]));
  }
  // 阶梯查询父级元素
  function getParentNode(el) {
    try {
      if (el.className === "") return getParentNode(el.parentNode);
      if (el.className.includes(targetName$1)) return el;
      var parentName = el.parentNode.className;
      if (parentName.includes(targetName$1)) return getParentNode(el.parentNode);
      if (!parentName.includes(targetName$1)) return el.parentNode;
    } catch (error) {
      console.log(error);
      console.log(el.className);
      return undefined;
    }
  }
  // 时间选择传值
  function checkSetTime(index, val) {
    var newList = _toConsumableArray(formList);
    newList[index].value = moment(val._d).format("YYYY-MM-DD");
    setList(newList);
  }
  // 时间区间选择传值
  function checkSetArrTime(index, val) {
    var param;
    if (val === null) param = val;else if (val[1] != null) param = moment(val[1]._d).format("YYYY-MM-DD");else param = moment(val[0]._d).format("YYYY-MM-DD");
    var newList = _toConsumableArray(formList);
    newList[index].value = param;
    setList(newList);
  }
  // 表单传值
  function checkNoSpace(index, e) {
    var param = e && e.target ? e.target.value : e;
    var newList = _toConsumableArray(formList);
    newList[index].value = param;
    setList(newList);
  }
  React.useEffect(function () {
    if (valid) dargAddFn();
    if (dragEnd) {
      draging$1 = null;
      targetIndex$1 = -1;
    }
  }, [dragEnd]);
  return /*#__PURE__*/React.createElement("div", {
    onDragEnter: function onDragEnter(e) {
      if (valid) return;
      e.preventDefault();
      setValid(true);
    },
    id: "uiform",
    className: "uiform"
  }, /*#__PURE__*/React.createElement(antd.Form, {
    form: props.form
  }, renderForm(formList)));
};

var css_248z$1 = ".catalogue {\r\n    width: 15%;\r\n    border: 1px solid #f1e8e8;\r\n    padding: 15px;\r\n    height: 100%;\r\n    box-sizing: border-box;\r\n    overflow: auto;\r\n}\r\n.catalogue-item {\r\n        display: flex;\r\n        align-items: center;\r\n        justify-content: center;\r\n        margin-bottom: 5px;\r\n        cursor: move;\r\n        color: #000;\r\n        user-select: none;\r\n        padding: 8px 10px;\r\n        background: #f6f7ff;\r\n        font-size: 12px;\r\n        cursor: move;\r\n        border: 1px dashed #f6f7ff;\r\n        border-radius: 3px;\r\n    }\r\n    .catalogue> div:last-child {\r\n        margin-bottom: 0;\r\n    }\r\n\r\n/*定义滚动条高宽及背景 高宽分别对应横竖滚动条的尺寸*/\r\n::-webkit-scrollbar {\r\n    width: 8px;\r\n    /*滚动条宽度*/\r\n    height: 8px;\r\n    /*滚动条高度*/\r\n    background-color: white;\r\n  }\r\n  \r\n  /*定义滑块 内阴影+圆角*/\r\n  ::-webkit-scrollbar-thumb {\r\n    box-shadow: inset 0 0 0px white;\r\n    -webkit-box-shadow: inset 0 0 0px white;\r\n    background-color: rgb(193, 193, 193);\r\n    /*滚动条的背景颜色*/\r\n    border-radius: 20px;\r\n  }";
styleInject(css_248z$1);

var example = [{
  label: "单行文本",
  // 标签名称
  tips: "请输入",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "Input",
  // 控件类型
  value: "",
  allowClear: true //是否允许清除-默认true
}, {
  label: "多行文本",
  // 标签名称
  tips: "请输入",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "InputTextera",
  // 控件类型
  value: "",
  allowClear: true //是否允许清除-默认true
}, {
  label: "数字框",
  // 标签名称
  tips: "请输入",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "InputNumber",
  // 控件类型
  value: null
}, {
  label: "下拉框",
  // 标签名称
  tips: "请输入",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "Select",
  // 控件类型
  options: [{
    label: "选项1",
    value: 1
  }],
  // 控件选项参数名称
  value: ""
}, {
  label: "单选",
  // 标签名称
  tips: "请输入",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "Radio",
  // 控件类型
  options: [{
    label: "选项1",
    value: 1
  }],
  // 控件选项参数名称
  value: ""
}, {
  label: "复选框",
  // 标签名称
  tips: "请输入",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "Checkbox",
  // 控件类型
  options: [{
    label: "选项1",
    value: 1
  }],
  // 控件选项参数名称
  value: ""
}, {
  label: "开关",
  // 标签名称
  tips: "开启",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "Switch",
  // 控件类型
  value: false
}, {
  label: "日期选择",
  // 标签名称
  tips: "请选择",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "DatePicker",
  // 控件类型
  value: ""
}, {
  label: "日期区间",
  // 标签名称
  tips: "请选择",
  // 提示语
  rule: "validateNone",
  // 需要限制规则名称
  type: "RangePicker",
  // 控件类型
  value: []
}];

var Config$1 = function Config(props) {
  var _useState = React.useState(example),
    _useState2 = _slicedToArray(_useState, 2),
    list = _useState2[0];
    _useState2[1];
  // fn 拖动开始
  var dragStartFn = function dragStartFn(item) {
    // 自动识别配置必要参数
    var param = _objectSpread2(_objectSpread2({}, item), {}, {
      required: false //是否必填
    });

    props.setNewParams(param);
    props.setDragEnd(false);
  };
  // fn 拖动结束
  var dragEndFn = function dragEndFn() {
    props.setDragEnd(true);
  };
  var renderList = function renderList() {
    return list.map(function (item, index) {
      return /*#__PURE__*/React.createElement("div", {
        onDragStart: function onDragStart() {
          return dragStartFn(item);
        },
        className: "catalogue-item",
        draggable: "true",
        onDragEnd: dragEndFn,
        key: index
      }, item.label);
    });
  };
  return /*#__PURE__*/React.createElement("div", {
    onDragEnter: function onDragEnter(e) {
      if (!props.valid) return;
      e.preventDefault();
      props.setValid(false);
    },
    className: "catalogue"
  }, renderList());
};

var css_248z = "\r\n.config {\r\n    width: 28%;\r\n    border: 1px solid #f1e8e8;\r\n   \r\n}\r\n.configContent{\r\n    height: 85%;\r\n    overflow: auto;\r\n    padding: 15px;\r\n}\r\n.delbtn{\r\n    width: 100%;\r\n    border-top: 1px solid #f1e8e8;\r\n    text-align: center;\r\n    line-height: 2.8;\r\n}\r\n.config .config-item {\r\n        margin-bottom: 10px;\r\n        padding-bottom: 15px;\r\n        border-bottom: 1px solid rgba(0, 0, 0, 0.07);\r\n        text-align: left;\r\n}\r\n.config .config-item-title {\r\n            color: #666;\r\n        }\r\n        .config  .config-item-option {\r\n            display: flex;\r\n            align-items: center;\r\n            justify-content: space-between;\r\n            margin-bottom: 5px;\r\n            padding: 5px;\r\n            user-select: none;\r\n            cursor: move;\r\n        }\r\n        .config   .config-item-option input {\r\n                border: none;\r\n                outline: none;\r\n                padding: 5px;\r\n                width: 28%;\r\n                background-color: #fff;\r\n                border: 1px solid #d9d9d9;\r\n                border-radius: 5px;\r\n            }\r\n            .config  .config-item-option-edit {\r\n                display: flex;\r\n                align-items: center;\r\n            }\r\n            .config    .config-item-option-edit > span {\r\n                    margin-left: 5px;\r\n                }\r\n            \r\n                .config    .config-item-option-disa {\r\n                cursor: pointer;\r\n                color: #000;\r\n                }\r\n                .config   .config-item-option-disa:hover {\r\n                    color: #0958d9;\r\n                }\r\n            \r\n                .config     .config-item-option-disabled {\r\n                cursor: not-allowed;\r\n                color: #ccc;\r\n            }\r\n            .config   .config-item-option-del {\r\n                color: #000;\r\n                cursor: pointer;\r\n            }\r\n            .config  .config-item-option-del:hover {\r\n                color: red;\r\n                cursor: pointer;\r\n            }\r\n\r\n/*定义滚动条高宽及背景 高宽分别对应横竖滚动条的尺寸*/\r\n::-webkit-scrollbar {\r\n    width: 8px;\r\n    /*滚动条宽度*/\r\n    height: 8px;\r\n    /*滚动条高度*/\r\n    background-color: white;\r\n  }\r\n  \r\n  /*定义滑块 内阴影+圆角*/\r\n  ::-webkit-scrollbar-thumb {\r\n    box-shadow: inset 0 0 0px white;\r\n    -webkit-box-shadow: inset 0 0 0px white;\r\n    background-color: rgb(193, 193, 193);\r\n    /*滚动条的背景颜色*/\r\n    border-radius: 20px;\r\n  }";
styleInject(css_248z);

var conditionContainer = function conditionContainer(props) {
  return props.show ? props.children : "";
};

var _this$1 = undefined;
var draging = null;
var targetName = "config-item-option"; //需要换位目标
var targetIndex = -1; //当前换位位置
var prevIndex = -1; //原坐标
var Option = antd.Select.Option;
var Config = function Config(props) {
  var _formList$active4, _formList$active5, _formList$active6, _formList$active7, _formList$active8;
  var active = props.active,
    formList = props.formList,
    setList = props.setList,
    setActive = props.setActive;
  // fn 拖动开始
  function dragStartFn(index, e) {
    e.dataTransfer.setData("te", e.target.innerText); //不能使用text，firefox会打开新tab
    draging = e.target;
    prevIndex = index;
  }
  // fn 拖动中
  function dragOverFn(index, e) {
    e.preventDefault();
    var target = getParentNode(e.target);
    if (!target || target.className !== targetName) return;
    if (target !== draging && draging) {
      //getBoundingClientRect()用于获取某个元素相对于视窗的位置集合
      target.getBoundingClientRect();
      draging.getBoundingClientRect();
      if (target && target.animated) return;
      targetIndex = index;
    }
  }
  // fn 拖动结束
  function dragEndFn() {
    var newList = _toConsumableArray(formList);
    var param = newList[active].options;
    var prev = param[prevIndex];
    var target = param[targetIndex];
    param.splice(prevIndex, 1, target);
    param.splice(targetIndex, 1, prev);
    newList[active].options = _toConsumableArray(param);
    setList(_toConsumableArray(newList));
  }
  // 阶梯查询父级元素
  function getParentNode(el) {
    if (el.className === "") return getParentNode(el.parentNode);
    if (el.className === targetName) return el;
    var parentName = el.parentNode.className;
    if (parentName !== targetName) return getParentNode(el.parentNode);
    if (parentName === targetName) return el.parentNode;
  }
  // 表单传值
  function changeValue(name, e) {
    var param = e && e.target ? e.target.value : e;
    var arr = _toConsumableArray(formList);
    arr[active][name] = param;
    setList(arr);
  }
  // 表单传值-option
  function changeOptionValue(index, e) {
    var value = e && e.target ? e.target.value : e;
    var arr = _toConsumableArray(formList);
    var param = arr[active].options;
    param[index].value = value;
    setList(arr);
  }
  function changeOptionLabel(index, e) {
    var value = e && e.target ? e.target.value : e;
    var arr = _toConsumableArray(formList);
    var param = arr[active].options;
    param[index].label = value;
    setList(arr);
  }

  // 数组转对象
  function arrToObj1(arr) {
    return arr.reduce(function (obj, item, index) {
      obj[index] = item;
      return obj;
    }, {});
  }
  // fn 删除控件
  function delItemFn() {
    var newList = _toConsumableArray(formList);
    newList.splice(active, 1);
    var newForm = newList.map(function (o) {
      return o.value;
    });
    var setform = {};
    if (newForm.length > 0) {
      setform = arrToObj1(newForm);
    }
    debugger;
    props.form.setFieldsValue(_objectSpread2({}, setform));
    setList(newList);
    setActive(-1);
  }
  // fn option 新增
  function addOptionFn() {
    var _formList$active, _formList$active$opti;
    var index = ((_formList$active = formList[active]) === null || _formList$active === void 0 ? void 0 : (_formList$active$opti = _formList$active.options) === null || _formList$active$opti === void 0 ? void 0 : _formList$active$opti.length) || 0;
    var param = {
      label: "\u9009\u9879".concat(index),
      value: index
    };
    var newList = _toConsumableArray(formList);
    var options = newList[active].options;
    newList[active].options = [].concat(_toConsumableArray(options), [param]);
    setList(newList);
  }
  // fn 删除 options
  function delOptionFn(index) {
    var _newList$active, _newList$active$optio;
    var newList = _toConsumableArray(formList);
    (_newList$active = newList[active]) === null || _newList$active === void 0 ? void 0 : (_newList$active$optio = _newList$active.options) === null || _newList$active$optio === void 0 ? void 0 : _newList$active$optio.splice(index, 1);
    setList(newList);
  }
  // fn 排序 options
  function orderOptionFn(index, desc, disabled) {
    var _newList$active2, _newList$active3, _newList$active3$opti, _newList$active4, _newList$active4$opti, _newList$active5, _newList$active5$opti;
    if (disabled) return; //排除无法换位的情况
    var newList = _toConsumableArray(formList);
    var param = (_newList$active2 = newList[active]) === null || _newList$active2 === void 0 ? void 0 : _newList$active2.options;
    var prev = param ? param[index] : "";
    var newIndex;
    switch (desc) {
      case true:
        newIndex = param ? param[index + 1] : "";
        (_newList$active3 = newList[active]) === null || _newList$active3 === void 0 ? void 0 : (_newList$active3$opti = _newList$active3.options) === null || _newList$active3$opti === void 0 ? void 0 : _newList$active3$opti.splice(index + 1, 1, prev);
        break;
      default:
        newIndex = param ? param[index - 1] : "";
        (_newList$active4 = newList[active]) === null || _newList$active4 === void 0 ? void 0 : (_newList$active4$opti = _newList$active4.options) === null || _newList$active4$opti === void 0 ? void 0 : _newList$active4$opti.splice(index - 1, 1, prev);
    }
    (_newList$active5 = newList[active]) === null || _newList$active5 === void 0 ? void 0 : (_newList$active5$opti = _newList$active5.options) === null || _newList$active5$opti === void 0 ? void 0 : _newList$active5$opti.splice(index, 1, newIndex);
    setList(newList);
  }
  // render options 选项卡
  function renderOption() {
    var _formList$active2,
      _formList$active2$opt,
      _this2 = this;
    return (_formList$active2 = formList[active]) === null || _formList$active2 === void 0 ? void 0 : (_formList$active2$opt = _formList$active2.options) === null || _formList$active2$opt === void 0 ? void 0 : _formList$active2$opt.map(function (item, index) {
      var _formList$active3, _formList$active3$opt;
      var max = (((_formList$active3 = formList[active]) === null || _formList$active3 === void 0 ? void 0 : (_formList$active3$opt = _formList$active3.options) === null || _formList$active3$opt === void 0 ? void 0 : _formList$active3$opt.length) || 1) - 1;
      return /*#__PURE__*/React.createElement("div", {
        onDragStart: dragStartFn.bind(_this2, index),
        onDragOver: dragOverFn.bind(_this2, index),
        onDragEnd: dragEndFn.bind(_this2),
        draggable: true,
        key: index,
        className: "config-item-option"
      }, /*#__PURE__*/React.createElement("input", {
        onChange: changeOptionLabel.bind(_this2, index),
        value: item.label,
        type: "text"
      }), /*#__PURE__*/React.createElement("input", {
        onChange: changeOptionValue.bind(_this2, index),
        type: "text",
        value: item.value
      }), /*#__PURE__*/React.createElement("div", {
        className: "config-item-option-edit"
      }, /*#__PURE__*/React.createElement(icons.ArrowUpOutlined, {
        className: "config-item-option-disa".concat(index === 0 ? "bled" : ""),
        onClick: orderOptionFn.bind(_this2, index, false, index === 0)
      }), /*#__PURE__*/React.createElement(icons.ArrowDownOutlined, {
        className: "config-item-option-disa".concat(index === max ? "bled" : ""),
        onClick: orderOptionFn.bind(_this2, index, true, index === max)
      })), /*#__PURE__*/React.createElement(icons.DeleteOutlined, {
        onClick: delOptionFn.bind(_this2, index),
        className: "config-item-option-del"
      }));
    });
  }
  return /*#__PURE__*/React.createElement("div", {
    className: "config"
  }, /*#__PURE__*/React.createElement("div", {
    className: "configContent"
  }, /*#__PURE__*/React.createElement(conditionContainer, {
    show: active !== -1
  }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "config-item"
  }, /*#__PURE__*/React.createElement("p", {
    className: "config-item-title"
  }, "\u6807\u7B7E"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(antd.Input, {
    allowClear: true,
    onChange: changeValue.bind(_this$1, "label"),
    placeholder: "\u8BF7\u8F93\u5165",
    value: (_formList$active4 = formList[active]) === null || _formList$active4 === void 0 ? void 0 : _formList$active4.label
  }))), /*#__PURE__*/React.createElement("div", {
    className: "config-item"
  }, /*#__PURE__*/React.createElement("p", {
    className: "config-item-title"
  }, "\u662F\u5426\u5FC5\u586B"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(antd.Switch, {
    onChange: changeValue.bind(_this$1, "required"),
    checked: (_formList$active5 = formList[active]) === null || _formList$active5 === void 0 ? void 0 : _formList$active5.required
  }))), /*#__PURE__*/React.createElement("div", {
    className: "config-item"
  }, /*#__PURE__*/React.createElement("p", {
    className: "config-item-title"
  }, "\u63D0\u793A\u8BED"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(antd.Input, {
    allowClear: true,
    onChange: changeValue.bind(_this$1, "tips"),
    placeholder: "\u8BF7\u8F93\u5165",
    value: (_formList$active6 = formList[active]) === null || _formList$active6 === void 0 ? void 0 : _formList$active6.tips
  }))), /*#__PURE__*/React.createElement("div", {
    className: "config-item"
  }, /*#__PURE__*/React.createElement("p", {
    className: "config-item-title"
  }, "\u9650\u5236\u89C4\u5219"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(antd.Select, {
    showSearch: true,
    onChange: changeValue.bind(_this$1, "rule"),
    placeholder: "\u8BF7\u9009\u62E9",
    allowClear: true,
    style: {
      width: '100%'
    },
    value: (_formList$active7 = formList[active]) === null || _formList$active7 === void 0 ? void 0 : _formList$active7.rule,
    filterOption: function filterOption(input, option) {
      return (option === null || option === void 0 ? void 0 : option.children).includes(input);
    }
  }, OptionsValidate.map(function (its) {
    return /*#__PURE__*/React.createElement(Option, {
      key: its.value,
      value: its.value
    }, its.label);
  })))), /*#__PURE__*/React.createElement(conditionContainer, {
    show: active !== -1 && ((_formList$active8 = formList[active]) === null || _formList$active8 === void 0 ? void 0 : _formList$active8.options) !== undefined
  }, /*#__PURE__*/React.createElement("div", {
    className: "config-item"
  }, /*#__PURE__*/React.createElement("p", {
    className: "config-item-title"
  }, "\u9009\u9879"), /*#__PURE__*/React.createElement(antd.Tag, {
    type: "primary",
    style: {
      marginBottom: '10px',
      cursor: 'pointer'
    },
    onClick: addOptionFn.bind(_this$1),
    color: "blue"
  }, "\u65B0\u589E\u9009\u9879"), /*#__PURE__*/React.createElement("div", null, renderOption())))))), /*#__PURE__*/React.createElement(conditionContainer, {
    show: active !== -1
  }, /*#__PURE__*/React.createElement("div", {
    className: "delbtn"
  }, /*#__PURE__*/React.createElement(antd.Button, {
    onClick: delItemFn.bind(_this$1),
    type: "primary"
  }, "\u5220\u9664\u7EC4\u4EF6")), "/"));
};

var _this = undefined;
var Drag = function Drag(props) {
  var _Form$useForm = antd.Form.useForm(null),
    _Form$useForm2 = _slicedToArray(_Form$useForm, 1),
    form = _Form$useForm2[0];
  var _useState = React.useState([]),
    _useState2 = _slicedToArray(_useState, 2),
    list = _useState2[0],
    setList = _useState2[1]; //列表
  var _useState3 = React.useState({}),
    _useState4 = _slicedToArray(_useState3, 2),
    newParams = _useState4[0],
    setNewParams = _useState4[1]; //当前拖拽新增元素
  var _useState5 = React.useState(true),
    _useState6 = _slicedToArray(_useState5, 2),
    dragEnd = _useState6[0],
    setDragEnd = _useState6[1]; //拖拽是否结束
  var _useState7 = React.useState(false),
    _useState8 = _slicedToArray(_useState7, 2),
    valid = _useState8[0],
    setValid = _useState8[1]; //拖拽是否有效
  var _useState9 = React.useState(-1),
    _useState10 = _slicedToArray(_useState9, 2),
    active = _useState10[0],
    setActive = _useState10[1]; //当前编辑控件下标

  var saveForm = function saveForm() {
    form.validateFields().then(function (res) {
      props.getlistChange(list);
    });
  };
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(antd.Space, {
    style: {
      position: 'absolute',
      left: '5px',
      top: '10px'
    }
  }, /*#__PURE__*/React.createElement(antd.Button, {
    type: "primary",
    onClick: saveForm
  }, "\u8868\u5355\u4FDD\u5B58"), /*#__PURE__*/React.createElement(antd.Button, {
    onClick: function onClick() {
      return setList([]);
    }
  }, "\u8868\u5355\u91CD\u7F6E")), /*#__PURE__*/React.createElement("div", {
    className: "drag",
    style: {
      width: props.width + 'px',
      height: props.height + 'px'
    }
  }, /*#__PURE__*/React.createElement(Config$1, {
    formList: list,
    setDragEnd: setDragEnd,
    setNewParams: setNewParams,
    setValid: setValid,
    valid: valid
  }), /*#__PURE__*/React.createElement(UiForm, {
    setActive: setActive.bind(_this),
    setList: setList.bind(_this),
    formList: list,
    newParams: newParams,
    dragEnd: dragEnd,
    valid: valid,
    form: form,
    active: active,
    setValid: setValid
  }), /*#__PURE__*/React.createElement(Config, {
    setActive: setActive.bind(_this),
    active: active,
    formList: list,
    form: form,
    setList: setList.bind(_this)
  })));
};

var Dynamicforms = function Dynamicforms(props) {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Drag, {
    getList: props.getlistChange,
    width: (props === null || props === void 0 ? void 0 : props.width) || 1000,
    height: (props === null || props === void 0 ? void 0 : props.height) || 800
  }));
};

exports.Dynamicforms = Dynamicforms;
