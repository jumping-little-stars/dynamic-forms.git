<h1 align="center">Welcome to 动态自定义表单组件 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.3-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> 你可以自由推拽组件，并且可以校验

### 组件效果

![Image text](https://gitee.com/jumping-little-stars/dynamic-forms/raw/master/assets/first.png)



```
```
### 组件介绍
**左侧**
多个表单控件，可自由选择拖拽至中间

**中间**
对推拽后的空间进行值的输入和选择

**右侧**
#### 基本设置，可设置控件标签，是否必填，校验规则
![Image text](https://gitee.com/jumping-little-stars/dynamic-forms/raw/master/assets/right1.png)

#### 校验规则有如下几种
![Image text](https://gitee.com/jumping-little-stars/dynamic-forms/raw/master/assets/right2.png)

#### 多选，下拉，单选可动态设置每个选择的label以及值
![Image text](https://gitee.com/jumping-little-stars/dynamic-forms/raw/master/assets/right3.png)

最后可点击表单保存，也可以表单重置


## Install

```sh
cnpm i dynamic-customization-form
```

## 使用
```
  <Dynamicforms
   getlistChange={getlistChange}   //获取表单保存的值
   width={1300} //动态设置宽高
   height={650} />
```

## Author

👤 **llzm**

* Github: [可以点击进入git 查看组件效果或下载代码](https://gitee.com/jumping-little-stars/dynamic-forms.git)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_